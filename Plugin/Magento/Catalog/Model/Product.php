<?php
/**
 * Daniel Coull <d.coull@suttonsilver.co.uk>
 * 2019-2020
 *
 */

namespace SuttonSilver\PriceLists\Plugin\Magento\Catalog\Model;

class Product
{
    /**
     * @var \SuttonSilver\PriceLists\Model\PriceListData 
     */
    protected $priceListData;

    /**
     * Product constructor.
     * @param \SuttonSilver\PriceLists\Model\PriceListData $priceListData
     */
    public function __construct(\SuttonSilver\PriceLists\Model\PriceListData $priceListData)
    {
        $this->priceListData = $priceListData;
    }

    /**
     * @param \Magento\Catalog\Model\Product $subject
     * @param $result
     * @return int
     */
    public function afterGetPrice(
        \Magento\Catalog\Model\Product $subject,
        $result
    ) {
        $price = $result;
        if ($this->priceListData->getGeneralConfig('enable')) {
            $custId = $subject->getCustomerId();
            $newPrice = $this->priceListData->getProductPrice($subject->getId(), $custId);
            $price = $newPrice;
        }

        return $price > 0 ? $price : $result;
    }
}
